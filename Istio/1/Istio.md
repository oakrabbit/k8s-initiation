# ISTIO

+ Check cluster
  ```kubectl cluster-info ```


1. init istio by running: 
  ```kubectl apply -f 1-istio-init.yaml ```
2. install Istio system : 
  ```kubectl apply -f 2-istio-minikube-reduced.yaml```
3. set Kiali secret : ``` kubectl apply -f 3-kiali-secret.yaml   ```
  
  cheack all running services in Istio-system Namespace :

  ```kubectl get svc -n istio-system ```

# Kiali
open url: http://kubernetes.docker.internal:31000/

U: admin
P: admin

# Jaeger 
Open : http://kubernetes.docker.internal:31001

# Grafana
Open : http://kubernetes.docker.internal:31002/


# Gateway demo

1- Install Files in the given order. 
2 - Open http://kubernetes.docker.internal:30080/  To see portal
OR see the URL using direct service: http://127.0.0.1:8001/api/v1/namespaces/default/services/http:fleetman-webapp:80/proxy/
This goes directly to the internal service. 

3 - install gateway files 


open : http://kubernetes.docker.internal/