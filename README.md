#Kubernetes Bootstrap

## 1. Setup Kubernetes


+ check if is running
  ``kubectl get svc`` 
   Lists services in defaut(or selected) namespace.
+ To be able to se full configuration do:
    ``kubectl config view``
+ gets current context for the kubectl config
``kubctl config current-context``
+ change Context to desired cluster
  ``kubectl config use-context <name>``
+ get cluster information
    ``kubectl cluster-info``
---
### 1.1 Setup dashboard if using Docker Kubernetes:

Source doc: https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/

  + kubernetes does not add Dashboard UI by default so we must add it to the cluster:
  ``kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta8/aio/deploy/recommended.yaml
``
  
  + To access dashboard now run : 
  ``kubectl proxy``
    URL : http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/login
+ once in the Dashboard login we can setup a config file with users to access it, or we use a token, that represents a service account to do so follow this :
  
  1- create the Admin user, in this case the administration serviceacccount. And a Rule to access the cluster.
  https://github.com/kubernetes/dashboard/wiki/Creating-sample-user
  2 - get the secrects for recent service account creation
  ``kubectl -n kube-system get secret | grep admin-user``
  3 - get the token for dashboard from the secret
  ``kubectl describe secret <secretname> -n kube-system``

```
Source doc: https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md 
Create user the fast way : 
Bash :
kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')

Powershell :
kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | sls admin-user | ForEach-Object { $_ -Split '\s+' } | Select -First 1)
```

---
### 1.2 API Server
+ to get the full API spec as open Api use: http://127.0.0.1:8001/swagger.json and then insert in swagger editor to view.
  
##

---
## 2 Resources
### 2.1 Pods
+ create a new pod based on the config file pod-demo.yaml.
 ``kubectl apply -f 2-pod-demo.yaml``
``` yaml 
apiVersion: v1  (1)
kind: Pod       (2)
metadata:       (3)
  name: poddemo
  labels:
    name: poddemo
spec:       (4)
  restartPolicy: "OnFailure"
  maxRetries: "3"
  containers:
  - name: poddemo
    image: heroku/nodejs-hello-world
    resources: (5)
      limits:
        memory: "128Mi"
        cpu: "500m"
    ports:
      - containerPort: 3000
    env:
      - name: PORT
        value: "5001" 
```
1. **apiVersion:**  Which version of the Kubernetes API you’re using to create this object.
2. **kind:** What kind of object you want to create.
3. **metadata:** Information that uniquely identify the object we are creating. Name or namespace, labels.
4. **spec** This is where we specify config of our pod e.g image name, container name, volumes, environment variables etc.
5. **resources:** he Container specifies a memory request, but not a limit. in this scenario we set the resources for 0.5(or 50% of 1 cpu) CPUs and Memory will be 128 Mebibyte or ~134MB.
   *Resources needs further reading from the teams to fully understand it.*
```
Mebi = 220
1 Mebibyte = 220 bytes
1 Mebibyte = 1,048,576 bytes

reference : https://www.gbmb.org/mib-to-mb
```

+ Pods Phase
  
| Value | description |
| -   | - |
|Pending  | The Pod has been accepted by the Kubernetes system, but one or more of the Container|
| Running | The Pod has been bound to a node, and all of the Containers have been created. |
| Succeeded | All Containers in the Pod have terminated in success, and will not be restarted. | 
| Failed |  All Containers in the Pod have terminated, and at least one Container has terminated |
| Unknown | For some reason the state of the Pod could not be obtained, typically due to an error in communicating with the host of the Pod.| 
|Completed |  The pod has run to completion as there’s nothing to keep it running eg. Completed Job|
| CrashLoopBackOff | This means that one of the containers in the pod has exited unexpectedly|



 + This pod has no service associated therefore it is only visible inside the kubernetes internal network.
+ for debugging purposes it is possible to portforward to the pod.
  ``kubectl port-forward POD_NAME HOST_PORT:POD_PORT``

  if using port forward access can then be done after running : 
  ``kubectl port-forward poddemo  8081:3000``
  using the URL : http://127.0.0.1:8081/
  or using kube proxy we can also access using the address:

  ``http://127.0.0.1:8001/api/v1/namespaces/bootstrap/pods/http:poddemo:5001/proxy/``


  Template for accessing pods / services via proxy :
  ```http://kubernetes_master_address/api/v1/namespaces/namespace_name/services/[https:]service_name[:port_name]/proxy ```
---
### 2.2 Namespaces
Kubernetes supports multiple virtual clusters backed by the same physical cluster. These virtual clusters are called namespaces.
+ create a new name space : 
  ``kubectl create ns bootstrap`` 
  this creates a namespace with no labes or annotations.
+ Create namespace using a config file (recomended due to versioning in git) : 
  ``kubectl apply -f bootstrap-namespace.yaml``
+ List all namespaces to see all namespaces:
  ``kubectl get ns``
+ Describe recent created name space:
  ``kubectl describe ns bootstrap output yaml``
  
``` yaml
apiVersion: v1
kind: Namespace
metadata:
  name:  bootstrap
  labels:
    type: demo
    owner: celfocus
```
![NameSpace](./images/namespace.png)
---
### 3 Deployments
To explain what a deployment is and wy we should use it lets create a new namespace to isolate from bootstrap namespace.
1. Build the new images beforehand from the helloworld deployment.
2.  create a new namespace called my-namespace-3.
   ``kubectl apply -f 3/my-namespace3.yaml ``
3. create a new deployment configuration :
``` yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: pingpong
  namespace: my-namespace-3
spec:
  replicas: 2
  selector:
    matchLabels:
      app: myhello3
  template:
    metadata:
      labels:
        app: myhello3
        release: stable
    spec:
      containers:
      - name: myhello3
        image:  oakrabit/hellowird:0.1
        imagePullPolicy: Never
        resources:
          limits:
            memory: "128Mi"
            cpu: "50m"
        ports:
          - containerPort: 3005
        env:
          - name: PORT
            valueFrom: 
              configMapKeyRef:
                name: helloworld-deployment-configs
                key: HELLO-PORT
```
More files have been created inside folder /3.
4. run : ``kubectl apply -f 3/helloworld-deployment.yaml ``
   A new delpoyment file is applyed with a replica set of 2 pods.
5. for a better understadment run: 
   ``kubectl describe deployment pingpong``

#### 3.1 Let's do a canary release
1. create a new deployment with a new version of the pod: oakrabit/hellowird:0.2.
2. Deploy the new deployment resource: 
```yaml

apiVersion: apps/v1
kind: Deployment
metadata:
  name: pingpong-canary #(1)
  namespace: my-namespace-3
spec:
  replicas: 1 
  selector:
    matchLabels:
      app: myhello3
  template:
    metadata:
      labels:
        app: myhello3
        release: canary #(2)
    spec:
      containers:
      - name: myhello3
        image: oakrabit/hellowird:0.2
        imagePullPolicy: Never
        resources:
          limits:
            memory: "128Mi"
            cpu: "50m"
        ports:
          - containerPort: 3005
        env:
          - name: PORT
            valueFrom: 
              configMapKeyRef:
                name: helloworld-deployment-configs
                key: HELLO-PORT
          
```
3. run :
   ``kubectl apply -f 3/helloworld-deployment-canary.yaml ``
4. Test it on the browser via proxy since the service is of type ClusterIp.
   URL : http://127.0.0.1:8001/api/v1/namespaces/my-namespace-3/services/http:hello3:8089/proxy/
   We can see that randomly the 2 version are returned to the client.
   Check running pods.
   ``kubectl get pods --show-labels``
5. Deployment do a rolling update strategy that means we can roolback 1 change
```note
A Deployment’s revision is created when a Deployment’s rollout is triggered. 
This means that the new revision is created if and only if the 
Deployment’s pod template (.spec.template) is changed, for example if you update the labels or container images of the 
template. 
Other updates, such as scaling the Deployment, do not create a Deployment revision, so that you can facilitate simultaneous manual or auto-scaling. 
This means that when you roll back to an earlier revision, only the Deployment’s pod template part is rolled back.
```
6. First we can check the rollout status by running: 
   ``kubectl rollout status deployment.v1.apps/pingpong-canary``
7. check history of rollout
   ``kubectl rollout history deployment.v1.apps/pingpong-canary``
  A list of revisions is returned as a result.
8. Using a deployment we can rollback in case something goes wrong. 
   to do so we can use the following command: 
   ``kubectl rollout undo  deployment.v1.apps/pingpong-canary``
   To go back to the previouse revision
   or
   ``kubectl rollout undo  deployment.v1.apps/pingpong-canary --revision=1``


###Helm
install helm by following the steps in:
``helm init --history-max 50``

## Missing :
- StatefullSets
- Persistent Volumes
- Ingress
- Helm charts
- 