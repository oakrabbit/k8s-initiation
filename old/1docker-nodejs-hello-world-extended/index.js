var express = require('express')
var app = express();


const USERNAME = process.env.USERNAME || 'admin'
const PASSWORD = process.env.PASSWORD || 'passworddefault'

//feature toggle 
const FEATURE1 = process.env.FEATURE1 || 'active'


app.set('port', (process.env.PORT || 5000))
app.use(express.static(__dirname + '/public'))

app.get('/', function(request, response) {
  response.send('Hello World! canary release')
})

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'))
})
